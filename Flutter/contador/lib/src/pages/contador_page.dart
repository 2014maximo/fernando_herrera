import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() => _ContadorPageState();
}

class _ContadorPageState extends State<ContadorPage> {
  final _estiloTexto = new TextStyle(fontSize: 25);

  int _count = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stateful'),
        centerTitle: true,
        elevation: 0.6,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Número de clicks:', style: _estiloTexto),
            Text('$_count', style: _estiloTexto)
          ],
        ),
      ),
      floatingActionButton: _crearBotones()
    );
  }

  Widget _crearBotones(){
    
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox( width: 30),
        FloatingActionButton( onPressed: _reset, child: Icon(Icons.exposure_zero)),
        Expanded( child: SizedBox()),
        FloatingActionButton( onPressed: _sustraer, child: Icon(Icons.remove)),
        SizedBox( width: 5.0),
        FloatingActionButton( onPressed: _agregar, child: Icon(Icons.add)),
      ]
    );
    
  }

  void _agregar(){
    setState(() =>
    _count ++
        );
  }

  void _sustraer(){
    setState(()=> _count --);
  }

  void _reset(){
    setState(() => _count = 0);
  }
}
